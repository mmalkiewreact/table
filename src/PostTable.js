import React from 'react';
import PostRow from './PostRow';

export default class PostTable extends React.Component {

    render() {
        var postFilter = this.props.postFilter;
        var posts = [];
        if(this.props.posts.length > 0) {
            posts = this.props.posts.map(function(post) {
                if (post.title.indexOf(postFilter) === -1) {
                    return;
                }
                return (<PostRow post={post} key={post.id}/>)
            });
        }
        return (
            <div>
                <table>
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>userId</th>
                        <th>title</th>
                        <th>body</th>
                    </tr>
                    </thead>

                    <tbody>
                    {posts}
                    </tbody>

                </table>
            </div>
        );

    }

}