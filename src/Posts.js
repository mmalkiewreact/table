import React from 'react';
import Search from './Search';
import PostTable from './PostTable';

export default class Posts extends React.Component {

    constructor(props) {
        super(props);

        this.state = {};
        this.state.posts = [];
        this.state.postFilter = "";
    }


    componentDidMount() {
        this.fetchPosts();

    }

    fetchPosts() {
        fetch('https://jsonplaceholder.typicode.com/posts')
            .then(results => {
                return results.json();
            })
            .then(data => {
                let posts = data.slice(0, 10);
                console.log("posts: " + posts);
                this.setState({ posts: posts })
            })
    }

    handleUserInput(postFilter) {
        this.setState({postFilter: postFilter});
    };

    render() {

        return (
            <div>
                <Search postFilter={this.state.postFilter} onUserInput={this.handleUserInput.bind(this)}/>
                <PostTable posts={this.state.posts} postFilter={this.state.postFilter}/>
            </div>
        );

    }
}