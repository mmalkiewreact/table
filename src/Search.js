import React from 'react';

export default class Search extends React.Component {

    handleChange() {
        this.props.onUserInput(this.refs.postFilter.value);
    }

    render() {
        return (
            <div>
                <input type="text"
                       placeholder="Search post"
                       value={this.props.postFilter}
                       ref="postFilter"
                       onChange={this.handleChange.bind(this)}/>
            </div>
        );
    }

}