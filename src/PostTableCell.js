import React from 'react';

export default class PostTableCell extends React.Component {

    render() {
        return (
            <td>
                <input type='text'
                       id={this.props.postData.id}
                       name={this.props.postData.type}
                       value={this.props.postData.value}/>
            </td>
        );

    }

}