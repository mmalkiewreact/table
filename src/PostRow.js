import React from 'react';
import PostTableCell from "./PostTableCell";

export default class PostRow extends React.Component {

    render() {

        return (
            <tr className="eachRow">
                <PostTableCell postData={{
                    "type": "id",
                    value: this.props.post.id,
                    id: this.props.post.id
                }}/>
                <PostTableCell postData={{
                    type: "userId",
                    value: this.props.post.userId,
                    id: this.props.post.id
                }}/>
                <PostTableCell postData={{
                    type: "title",
                    value: this.props.post.title,
                    id: this.props.post.id
                }}/>
                <PostTableCell postData={{
                    type: "body",
                    value: this.props.post.body,
                    id: this.props.post.id
                }}/>
            </tr>
        );

    }

}